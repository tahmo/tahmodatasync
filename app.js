require('console-stamp')(console, '[yyyy-mm-dd HH:MM:ss.l]');

var request     	= require('request'),
	config 			= require('./config.js'),
    fs          	= require('fs'),
    _ 				= require('lodash'),
    $q 				= require('q'),
    moment 			= require('moment'),
    tahmoAPI 		= require('./routes/api_wrapper.js'),
    csvExport 		= require('./routes/csv_export.js');

var lastMeasurementCache 	= {};
var quickMode 				= false;

var syncData 				= function()
{
	var deferred 		= $q.defer();
	var maxRequests 	= 10;

	// Set quickmode to false, will be set to true if a station requires syncing for multiple days
	quickMode 			= false;

	// Get stations
	tahmoAPI.getStations()
		.then(function(stations)
		{
			// Write stations to file
			fs.writeFileSync('./data/stations.json', JSON.stringify(stations));

			if(config.EXPORT_CSV)
			{
				csvExport.stations('./data/stations.csv', stations);
			}

			var syncTasks 	= [];

			// Check per station for dates that haven't been synched yet
			console.log('Checking data for', stations.length, 'stations');
			_.forEach(stations, function(station)
			{
				var dates 	= getStationSyncDates(station);
				if(_.size(dates) > 0 && _.size(syncTasks) < maxRequests)
				{
					syncTasks.push({ 'station': station, 'date': dates[0] });
				}
			});

			// Create promises for all scheduled synchronisation tasks
			console.log(_.size(syncTasks), 'scheduled syncing tasks');
			var syncTasksPromises 	= [];
			_.forEach(syncTasks, function(task)
			{
				syncTasksPromises.push(syncStationDate(task.station, task.date));
			});

			// Wait for all synchronisation taks to complete before fulfilling the promise
			$q.all(syncTasksPromises)
				.then(function()
				{
					deferred.resolve();
				})
				.catch(function(err)
				{
					deferred.reject(err);
				});
		})
		.catch(function(err)
		{
			deferred.reject(err);
		});

	return deferred.promise;
}

var getStationSyncDates 	= function(station)
{
	var datesToSync = []

	// Check if last measurement property of station is defined
	if(_.has(station, 'lastMeasurement') && station.lastMeasurement.length > 0)
	{
		// Check if measurements should be updated for this station
		if(!_.has(lastMeasurementCache, station.id) || lastMeasurementCache[station.id] != station.lastMeasurement)
		{
			var dir 	= './data/' + station.id;

			// Create dir is not availabe
			if(!fs.existsSync(dir))
			{
				fs.mkdirSync(dir);
				fs.mkdirSync(dir + '/json');

				if(config.EXPORT_CSV)
				{
					fs.mkdirSync(dir + '/csv');
				}
			}

			// Get most recent file for this station
			var files 		= fs.readdirSync(dir + '/json').sort();
			var lastDate 	= (files.length == 0) ? ((new Date(station.firstMeasurement.substring(0, 10)) < new Date(config.START_DATE)) ? config.START_DATE : station.firstMeasurement.substring(0, 10)) : _.last(files).slice(0,-5);	// Remove .json from filename

			// Get data for each date between the last file date and the last measurement
			var a 			= (lastDate.length) ? moment(lastDate) : moment(station.firstMeasurement.substring(0, 10));
			var b 			= moment(station.lastMeasurement.substring(0, 10));

			if(!_.has(lastMeasurementCache, station.id) || (Math.abs(a.diff(b, 'days')) == 0 && station.lastMeasurement != lastMeasurementCache[station.id]))
			{
				datesToSync.push(a.format('YYYY-MM-DD'));
			}
			else if(b.diff(a, 'days') >= 1)
			{
				datesToSync.push(a.add(1, 'days').format('YYYY-MM-DD'));
			}

			if(Math.abs(a.diff(b, 'days')) > 1)
			{
				quickMode 		= true;
			}
		}
	}

	return datesToSync;
}

var syncStationDate 		= function(station, date)
{
	var deferred 		= $q.defer();
	console.log('[' + station.id + ']', 'Retrieving data for', date);

	try {
		var dir 	= './data/' + station.id;

		// Get raw measurements for the specified date.
		tahmoAPI.getRawMeasurements(station.id, { 'startDate': date, 'endDate': date + 'T23:59' })
			.then(function(timeseries)
			{
				console.log('[' + station.id + ']', 'Write data to json for', date);
				fs.writeFileSync(dir + '/json/' + date + '.json', JSON.stringify(timeseries));

				if(config.EXPORT_CSV)
				{
					console.log('[' + station.id + ']', 'Write data to csv for', date);
					csvExport.timeseries(dir + '/csv/' + date + '.csv', timeseries);
				}

				// Update last measurement cache
				if(!_.has(lastMeasurementCache, station.id) || date > lastMeasurementCache[station.id])
				{
					lastMeasurementCache[station.id] 	= (date == station.lastMeasurement.substring(0, 10)) ? station.lastMeasurement : date;
				}

				deferred.resolve();
			})
			.catch(function(err)
			{
				deferred.reject('Failed to get measurements');
			});

	}
	catch(err)
	{
		deferred.reject(err);
	}

	return deferred.promise;
}

// Continuesly perform main loop with 5 minute pauses in between unless there's a syncing backlog
var main 	= function()
{
	try {
		syncData()
		.catch(function(err)
		{
			console.log('Error');
			console.log(err);
		})
		.finally(function()
		{
			setTimeout(function()
			{
				main();
			}, (quickMode == true) ? 100 : 15 * 60 * 1000);
		});
	}
	catch(err)
	{
		console.log(err);
		console.log('Critical error, restart required');
	}
}

main();