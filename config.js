module.exports = {
	'API_KEY'		: '',
	'API_SECRET'	: '',
	'API_BASE_URL'	: 'https://tahmoapi.mybluemix.net/v1/',
	'EXPORT_CSV' 	: true,
	'CSV_UNKOWN' 	: '"NAN"',
	'CSV_DELIMITER' : ',',
	'CSV_LINE' 		: '\n',
	'START_DATE' 	: '2018-01-01'
}