var config 		= require('../config.js'),
	_			= require('lodash'),
	fs 			= require('fs');

module.exports 	= (function ()
{
	var stations		= function(path, stations)
	{
		var columns 	= {
			'Station ID'		: 'id',
			'Name'				: 'name',
			'Datalogger ID'		: 'deviceId',
			'Latitude' 			: 'location.lat',
			'Longitude' 		: 'location.lng',
			'Active' 			: 'active',
			'First Measurement' : 'firstMeasurement',
			'Last Measurement' 	: 'lastMeasurement'
		};

		fs.writeFileSync(path, _.map(_.keys(columns), function(columnName){ return '"' + columnName + '"'; }).join(config.CSV_DELIMITER));

		_.forEach(stations, function(station)
		{
			fs.appendFileSync(path, config.CSV_LINE + _.map(columns, function(property){ return '"' + (_.has(station, property) ? _.get(station, property) : '') + '"'; }).join(config.CSV_DELIMITER));
		});
	}

	var timeseries	= function(path, timeseries)
	{
		var types 			= _.keys(timeseries);
		var data 			= {};

		_.forEach(timeseries, function(series, type)
		{
			_.forEach(series, function(value, timestamp)
			{
				if(!_.has(data, timestamp))
				{
					data[timestamp] 	= {};
				}

				data[timestamp][type] 	= value;
			});
		});

		fs.writeFileSync(path, [ '"timestamp"' ].concat(_.map(types, function(type){ return '"' + type + '"'; })).join(config.CSV_DELIMITER));

		_.forEach(data, function(values, timestamp)
		{
			var line 	= [ timestamp ];
			_.forEach(types, function(type)
			{
				line.push(_.has(values, type) ? values[type] : config.CSV_UNKOWN);
			});

			fs.appendFileSync(path, config.CSV_LINE + line.join(config.CSV_DELIMITER));
		});
	}

	return {
		stations		: stations,
		timeseries		: timeseries
	};

})();