var config 		= require('../config.js'),
	_			= require('lodash'),
	request 	= require('request'),
	fs 			= require('fs'),
	$q 			= require('q'),
	querystring = require('querystring');

module.exports 	= (function ()
{

	var getHeaders				= function()
	{
		return { 'Authorization': 'Basic ' + new Buffer(config.API_KEY + ":" + config.API_SECRET).toString("base64") };
	}

	var sendRequest 			= function(endpoint, params)
	{
		var deferred 		= $q.defer();

		// Check optional parameters
		params 			= (typeof params === 'undefined' || !_.isObject(params)) ? {} : params;

		request(
		{
			'url'		: config.API_BASE_URL + endpoint + '?' + querystring.stringify(params), 
			'headers'	: getHeaders(), 
			'timeout' 	: 30000
		},
		function(error, response, body)
		{
			if(error)
			{
				deferred.reject(error);
				return false;
			}

			try {
				var jsonResponse 		= JSON.parse(body);

				if(!_.has(jsonResponse, 'status') || jsonResponse.status != 'success')
				{
					throw "API response unsuccesful";
				}

				deferred.resolve(jsonResponse);
			}
			catch(error)
			{
				deferred.reject(error);
				return false;
			}
		});

		return deferred.promise;
	}

	var getStations 			= function()
	{
		var deferred 		= $q.defer();

		sendRequest('stations')
			.then(function(response)
			{
				deferred.resolve(response.stations);
			})
			.catch(function(err)
			{
				deferred.reject('Failed to get stations');
			});

		return deferred.promise;
	}

	var getRawMeasurements		= function(stationId, params)
	{
		var deferred 		= $q.defer();

		sendRequest('timeseries/' + stationId + '/rawmeasurements', params)
			.then(function(response)
			{
				deferred.resolve(response.timeseries);
			})
			.catch(function(err)
			{
				deferred.reject('Failed to get measurements');
			});

		return deferred.promise;
	}

	return {
		getStations			: getStations,
		getRawMeasurements	: getRawMeasurements
	};

})();